//Jimmy le 1936415
package test3_1936415;

public class Recursion {
	
	public static void main(String[] args) {
		
		String[] y = {"hello", "qqqqqq", "eq", "apple", "boop", "pq", "q"};
		
		int x = recursiveCount(y,0);
		System.out.println(x);
	}
	

	public static int recursiveCount(String[] words, int n) {
		if (n == words.length) {
			return 0;
		}
		
		if( n % 2 == 0 && words[n].contains("q")) {
			return recursiveCount(words,n+1) + 1;
		}
		return recursiveCount(words, n+1);
	}
}
