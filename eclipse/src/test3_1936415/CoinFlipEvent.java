//Jimmy Le 1936415
package test3_1936415;

import java.util.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.text.Text;

public class CoinFlipEvent implements EventHandler<ActionEvent>{
	
	private Text currency;
	private TextField display;
	private TextField input;
	private int choice;
	//used to determine if the user won or not
	private static boolean isWin;

	//constructor
	public CoinFlipEvent(Text currency, TextField display, TextField input, int choice) {
	
		this.currency = currency;
		this.display = display;
		this.input = input;
		this.choice = choice;
		
		
	}
	
	@Override
	public void handle(ActionEvent event) {
		isWin = false;
		String userInput = input.getText();
		String toBeDisplayed = "";
		try {
			//parse the input into a number
			int checkNumber = Integer.parseInt(userInput);
			//parse the currency into a number
			int currentAmount = Integer.parseInt(currency.getText());
			//check if the input is less than the currency that they currently have and more than 0
			if(checkNumber <= currentAmount && checkNumber > 0) {
				//if that is the case, run flip which returns the message to be displayed
				toBeDisplayed = flip(choice);
				//if the user wins, sum the current amount and the number that the user wanted to bet
				if(isWin) {
					currency.setText("" + (currentAmount + checkNumber));
				}
				//if the user loses, subtract the amount bet from the current currency
				else {
					currency.setText("" + (currentAmount - checkNumber));
				}
			}
			//if the user bet more than what they have, dont run the program, change the to be displayed to be an error message
			else{
				toBeDisplayed = "Invalid amount of currency";
			}
		}
		//if there is a problem such as the user entering a string instead of a number, catch it and change the tobedisplayed to be an error message
		catch(Exception e){
			toBeDisplayed = "Invalid input, please enter a number";
		}
		//change the textfield to be toBeDisplayed
		finally {
			display.setText(toBeDisplayed);
		}
	}
	
	//randomly generates a number, assign that number to a head/tails, return the result of the game as a message, and change the global static boolean depending on the outcome
	public static String flip(int choice) {
		Random randgen = new Random();
		int chance = randgen.nextInt(2);
		int heads = 0;
		int tails = 1;
		String result = "";
		
		//setting the initial part of the message, which is the result of the coin flip.
		if(chance == heads) {
			result = "It landed on Heads!";
		}
		else {
			result = "It landed on Tails!";
		}
		
		//changing the boolean value depending on the outcome, as well as setting the end part of the message to be the win/loss message.
		if(chance == choice) {
			isWin = true;
			result = result + " You Win!";
		}
		else {
			isWin = false;
			result = result + " You Lose!";
		}
		//return the resulting message
		return result;
		
	}

}












