//Jimmy Le 1936415
package test3_1936415;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CoinFlip extends Application{


	@Override
	public void start(Stage stage) throws Exception {
		
		//group
		Group root = new Group();
		
		//boxes
		VBox vb = new VBox();
		HBox hb = new HBox();
		HBox ht = new HBox();
		
		//textfields
		Text label = new Text("Current amount: ");
		Text currency = new Text("100");
		TextField input = new TextField("Please enter bet amount here:");
		TextField display = new TextField("Welcome!");
		
		input.setMinWidth(400);
		//buttons
		Button heads = new Button("Heads");
		Button tails = new Button("tails");
		
		
		//adding event on buttons
		int headchoice = 0;
		int tailchoice = 1;
		
		CoinFlipEvent he = new CoinFlipEvent(currency, display, input, headchoice);
		CoinFlipEvent te = new CoinFlipEvent(currency, display, input, tailchoice);
		
		heads.setOnAction(he);
		tails.setOnAction(te);
		//adding things together
		hb.getChildren().addAll(label, currency);
		ht.getChildren().addAll(heads, tails);
		vb.getChildren().addAll(hb,input,ht,display);
		root.getChildren().addAll(vb);
		
		//Scene
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.PINK);
		
		//Stage
		stage.setTitle("Gambling!");
		stage.setScene(scene);
		stage.show();
		
		
		
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	
	
}
